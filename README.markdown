# Driver for PLL CS2200-CP

## Description
 This is generic library for CS2200-CP PLL. Library is split into two parts:
 
 * High level driver
 * HAL driver
 
 Meanwhile high level driver is common for all platform, HAL is hardware
 depend. All HAL drivers should contain same functions, but implementation
 may be different.

## Files
 * cs2200.* - generic high level drivers for module. It need include HAL
 * cs2200_HAL_* - HAL driver

## Usage
 In cs2200.h, section "Included libraries" include correct HAL driver. For
 example:
  if you use AVR32 UC3A3 architecture, then type:
  #include "cs2200_HAL_AVR32_UC3A3_interface.h"
 When there is not HAL driver for your favorite MCU, then try write one ;)

## Get source code!
 `git clone https://MartinStejskal@bitbucket.org/MartinStejskal/cs2200-driver.git`

## Notes
 * HAL driver should contain only basic libraries
